

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	stock:{
		type: Number,
		required: [true, "Stocks is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	order : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			email:{
				type: String,
				required: [true, "Username is required"]
			},
			isPaid:{
				type: Boolean,
				default: true
			},
			dateOrdered:{
				type: Date,
				default : new Date() 
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);