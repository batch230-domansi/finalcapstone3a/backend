
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber:{
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	createOn:{
		type: Date,
		default: new Date()
	},
	order: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName:{
				type: String,
				required: [false, "Product name is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Successful"
			},
			isPaid: {
				type: Boolean,
				default: true
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);
