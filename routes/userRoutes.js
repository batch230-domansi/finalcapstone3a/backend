const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/register", userControllers.registerUser);

router.get("/allusers", userControllers.getAllUsers);

router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getProfile);

router.post("/checkout", auth.verify, userControllers.checkout);

router.get("/order", auth.verify, userControllers.getAllOrder);

module.exports = router;